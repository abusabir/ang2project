'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');

module.exports = {
    context: __dirname + '/frontend',
    entry: {
      main: ["webpack-dev-server/client?http://localhost:8080", "webpack/hot/dev-server", "./index.js"]
    },

    output: {
      path: __dirname + "/public",
      filename: "bundle.js"
    },

    watch: NODE_ENV == 'development',

    watchOptions: {
      aggregateTimeout: 100
    },

    devtool: NODE_ENV == 'development' ? "inline-source-map" : null, 

    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ],

    // resolve: {
    //   modulesDirectories: ['node_modules'],
    //   extensions: ['', ',js']
    // },

    // resolveLoader: {
    //   modulesDirectories: ['node_modules'],
    //   moduleTemplates: ['*-loader'],
    //   extensions: ['', '.js']
    // },

    module: {
     loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel', 
          query: {
            presets: ['es2015']
          }

        },
        {
            test: /\.css$/,
            loaders: ["style", "css", "autoprefixer"]
        }
      ]
    },

    devServer: {     
      contentBase: __dirname + '/public',
      hot: true
    }
};

if (NODE_ENV == 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false, 
        drop_console: true, 
        unsafe: true
      }
    })
  );
}